// @ts-check
const { test, expect } = require('@playwright/test');

test.beforeEach(async ({ page }) => {
  // await page.goto('https://demo.playwright.dev/todomvc');
    // await page.goto('https://account.collegeboard.org/login/login?appId=435&DURL=https://collegeprofile.collegeboard.org/bfcp-dashboard');
   await page.goto('https://collegeboard.vndly.com/sign_in');

});






test.describe('VNDY Login',()=>{
  test('should enter timesheet for today',async({page})=>{

    await page.locator('#username').fill("rakesh.k.gautam1990@gmail.com");
    await page.locator(':text("Continue")').click();
    await page.locator('#password').fill("Mukku@0107");
    await page.locator(':nth-match(:text("Sign In"), 1)').click();
    await expect(page).toHaveTitle("Timesheet Summary");

const dayOfWeekName = new Date().toLocaleString(
  'default', {weekday: 'long'}
);

const hours=8;
switch(dayOfWeekName) {
  case "Sunday":
    console.log("Today is "+dayOfWeekName+",Entered hours =0"); 
    await page.locator('tbody[class=addNew-sec]>tr:nth-child(2)>td:nth-child(5)>div>input').fill("0");
    break;
  case "Monday":
    await page.locator('tbody[class=addNew-sec]>tr:nth-child(2)>td:nth-child(6)>div>input').fill(hours.toString());
    console.log("Today is "+dayOfWeekName+",Entered hours ="+hours); 
    break;
  case "Tuesday":
    await page.locator('tbody[class=addNew-sec]>tr:nth-child(2)>td:nth-child(7)>div>input').fill(hours.toString());
    console.log("Today is "+dayOfWeekName+",Entered hours ="+hours); 
    break;
  case "Wednesday":
    await page.locator('tbody[class=addNew-sec]>tr:nth-child(2)>td:nth-child(8)>div>input').fill(hours.toString());
    console.log("Today is "+dayOfWeekName+",Entered hours ="+hours); 
    break;
  case "Thursday":
    await page.locator('tbody[class=addNew-sec]>tr:nth-child(2)>td:nth-child(9)>div>input').fill(hours.toString());
    console.log("Today is "+dayOfWeekName+",Entered hours ="+hours); 
    break;
  case "Friday":
    await page.locator('tbody[class=addNew-sec]>tr:nth-child(2)>td:nth-child(10)>div>input').fill(hours.toString());
    console.log("Today is "+dayOfWeekName+",Entered hours ="+hours); 
    break;
  default:
    console.log("Weekend hour needs to be filled manualluy"); 

}
await page.locator('button[data-testid="save"]').click();
await page.locator('Timesheet submitted successfullly')


  })
})














async function createDefaultTodos(page) {
  for (const item of TODO_ITEMS) {
    await page.locator('.new-todo').fill(item);
    await page.locator('.new-todo').press('Enter');
  }
}

/**
 * @param {import('@playwright/test').Page} page
 * @param {number} expected
 */
 async function checkNumberOfTodosInLocalStorage(page, expected) {
  return await page.waitForFunction(e => {
    return JSON.parse(localStorage['react-todos']).length === e;
  }, expected);
}

/**
 * @param {import('@playwright/test').Page} page
 * @param {number} expected
 */
 async function checkNumberOfCompletedTodosInLocalStorage(page, expected) {
  return await page.waitForFunction(e => {
    return JSON.parse(localStorage['react-todos']).filter(i => i.completed).length === e;
  }, expected);
}

/**
 * @param {import('@playwright/test').Page} page
 * @param {string} title
 */
async function checkTodosInLocalStorage(page, title) {
  return await page.waitForFunction(t => {
    return JSON.parse(localStorage['react-todos']).map(i => i.title).includes(t);
  }, title);
}
